﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Text;
using System.Diagnostics;
namespace mis_assignemtn2
{
    public partial class gorder : System.Web.UI.Page
    {
        string ufname = "";
        string ulname = "";
        int id; //empid
        int cid; //customer id
        int pid; //product id
        int oid; //order id
        SqlConnection conn = new SqlConnection();
        string constr = @"Server=tcp:dbnje.database.windows.net,1433;Initial Catalog=mydb;Persist Security Info=False;User ID=amman;Password=Pasha123_;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
        SqlCommand cmd = new SqlCommand();
        SqlCommand cmd1 = new SqlCommand();
        SqlDataSource SqlDataSource1 = new SqlDataSource();
        public static List<string> list1 = new List<string>();
        public static List<string> finalprodname = new List<string>();
        public static List<string> finalprodquan = new List<string>();
        public static List<string> Result2 = new List<string>();
        String[] str = new String[100];
        string str1;

        protected void Page_Load(object sender, EventArgs e)
        {
            cust_list.Focus();
            if(!IsPostBack)
            {
                finalprodname.Clear();
                finalprodquan.Clear();
                list1.Clear();
                conn.ConnectionString = constr;
                conn.Open();
                string query = "select empid from cuser"; 
                cmd.CommandText = query;
                cmd.Connection = conn;
                id = (int)cmd.ExecuteScalar(); 
                query = "select [fname] from [emp] where [empid] = " + id + "";
                cmd.CommandText = query;
                ufname = (string)cmd.ExecuteScalar();
                query = "select [lname] from [emp] where [empid] = " + id + "";
                cmd.CommandText = query;
                ulname += (string)cmd.ExecuteScalar();
                usrname_lbl.Text = "Logged-in as: " + ufname + " " + ulname + "<br />Employee Code: E00" + id;
                usrname_lbl.Font.Bold = true;
                /* order generation*/
                query = "select custname from customer";
                cmd.CommandText = query;
                SqlDataReader dr;
                dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        cust_list.Items.Add(dr[0].ToString()); //adding customers to dropdown

                    }
                }

                conn.Close();


                /* code for product gridview */
                SqlDataSource1.ID = "SqlDataSource1";
                this.Page.Controls.Add(SqlDataSource1);
                SqlDataSource1.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["GenerateOrderGridViewConnectionString"].ConnectionString;
                SqlDataSource1.SelectCommand = "SELECT productname from product";
                GridView1.DataSource = SqlDataSource1;
                GridView1.DataBind();
             
                
            }
            

            
            conn.ConnectionString = constr;
            conn.Open();
            cmd.Connection = conn;
            //getting empid 
            cmd.CommandText = "select empid from cuser";
            cmd.Connection = conn;
            id = (int)cmd.ExecuteScalar();
            //getting orderid
            cmd.CommandText = "select COALESCE(max(orderid), 0) FROM [order]";
            oid = (int)cmd.ExecuteScalar();
            oid = oid + 1; //incerase oid to next oid
            shit.Text = "<br /><br />current orderid = " + oid;
            conn.Close();
        }

        protected void logout_btn_Click(object sender, EventArgs e)
        {
            conn.ConnectionString = constr;
            conn.Open();
            string query = "delete from cuser";
            cmd.Connection = conn;
            cmd.CommandText = query;
            cmd.ExecuteNonQuery();
            conn.Close();
            Response.Redirect("index.aspx");
        
        }





        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DropDownList DropDownList1 = (e.Row.FindControl("DropDownList1") as DropDownList);
                DropDownList1.Items.Add("1");
                DropDownList1.Items.Add("2");
                DropDownList1.Items.Add("3");
                DropDownList1.Items.Add("4");
                DropDownList1.Items.Add("5");
            }

        }


        private int GetColumnIndexByName(GridView grid, string name)
        {
            foreach (DataControlField col in grid.Columns)
            {
                if (col.HeaderText.ToLower().Trim() == name.ToLower().Trim())
                {
                    return grid.Columns.IndexOf(col);
                }
            }

            return -1;
        }


        protected void Button2_Click(object sender, EventArgs e)
        {
            //Label2.Text += "<br />===========================================================================================<br />";
            foreach (GridViewRow row in GridView1.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox chkRow = (row.Cells[0].FindControl("chkRow") as CheckBox);
                    if (chkRow.Checked)
                    {
                        string name = (row.Cells[0].FindControl("DropDownList1") as DropDownList).Text;
                        int ind = GetColumnIndexByName(GridView1, "ProductName");

                        string country = row.Cells[ind].Text;
                        //Label2.Text += "<br />-----------------<br />You've selected frm dropdown: " + name + "<br />ProductName:  " + country + "<br />-----------------<br />";
                        //Label2.Text = "";

                        list1.Add(country + name);
                        str1 += country + name + ",";

                        foreach (string s in list1)
                        {
                            //Label2.Text += s + "<br />";
                        }
                        //Label2.Text = str1;

                    }
                }
            }
        }


        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "AddToCart")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                GridViewRow row = GridView1.Rows[index];
                Button btn = row.Cells[0].FindControl("AddButton") as Button;
                if (1==1) //testing, will remove this later
                {
                    btn.Text = "Added!";
                    CheckBox chkRow = (row.Cells[0].FindControl("chkRow") as CheckBox);
                    chkRow.Checked = true;
                    Button2_Click(null, null);
                }
                else
                {
                    btn.Text = "Add to Cart";
                    CheckBox chkRow = (row.Cells[0].FindControl("chkRow") as CheckBox);
                    chkRow.Checked = false;
                }

            }
        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            SqlDataSource1.SelectCommand = "SELECT productname from product";
            SqlDataSource1.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["GenerateOrderGridViewConnectionString"].ConnectionString;
            GridView1.DataSource = SqlDataSource1;
            GridView1.DataBind();
        }





   
        protected void ordersubmit_Click(object sender, EventArgs e)
        {

            conn.ConnectionString = constr;
            cmd.CommandText = "select count(*) from product where productquantity > 0";
            cmd.Connection = conn;
            conn.Open();
            //int arraysize = (int)cmd.ExecuteScalar();
            int pid;
            //getting customer id
            cmd.CommandText = "select custid from customer where custname = '" + cust_list.SelectedItem.Text + "'";
            cid = (int)cmd.ExecuteScalar();
            //conn.Close();
            //String[] selectedItemsNames = new String[arraysize];
            //int i = 0;
            int totalprod = finalprodname.Count();
         //   foreach (ListItem item in prod_list.Items)
         //   {
          //      if (item.Selected)
          //      {
        //            selectedItemsNames[i] = item.Value;
        //            i++;
       //         }
        //    }
            //create order in ordertable
            cmd.CommandText = "insert into [order]([orderid],[custid], [orderdate]) values('" + oid + "','" + cid + "','" + DateTime.Now.ToString("dd/MM/yyyy") + "')";
            cmd.ExecuteNonQuery();
            for (int j = 0; j < totalprod; j++)
            {
                cmd.CommandText = "select productid from product where productname ='" + finalprodname[j] + "'";
                pid = (int)cmd.ExecuteScalar();
                //add products in orderproduct
                cmd.CommandText = "insert into [orderproduct]([orderid], [productid], [quantity]) values ('" + oid + "','" + pid + "','" + finalprodquan[j] + "')";
                cmd.ExecuteNonQuery();
            }

            cmd.CommandText = "insert into sales(empid,orderid,commision) values('" + id + "','" + oid + "','0')";
            shit.Text += "<br /> " + "insert into sales(empid,orderid,commision) values('" + id + "','" + oid + "','0')";
            cmd.ExecuteNonQuery();
         
            //getting orderTotal

            cmd.CommandText = "SELECT SUM(product.ProductPrice* orderproduct.quantity) AS OrderTotal FROM ((product INNER JOIN orderproduct ON product.ProductID = orderproduct.ProductID) INNER JOIN [order] ON orderproduct.OrderID = [order].orderid) GROUP BY [order].orderid HAVING ([order].orderid = " + oid + ")";
            int orderTotal = Convert.ToInt32(cmd.ExecuteScalar());
            shit.Text = orderTotal + " = OrderTotal";
            //updating orderTotal in ordertable
            cmd.CommandText = "UPDATE [order] SET OrderTotal = " + orderTotal + " WHERE (orderid = " + oid +")";
            cmd.ExecuteNonQuery();
            //updating commision in Sales table
            cmd.CommandText = "UPDATE sales SET Commision = " + (orderTotal*0.1) +" WHERE (OrderID = " + oid + ")";
            shit.Text += "<br />UPDATE sales SET Commision = " + (orderTotal * 0.1) + " WHERE (OrderID = " + oid + ")";
            cmd.ExecuteNonQuery();
            //everything should be stored by now
            finalprodquan.Clear();
            finalprodname.Clear();
            list1.Clear();
            Result2.Clear();
            Response.Redirect("ordergenok.aspx?oid=" + oid);

        }

        protected void cartbtn_Click(object sender, EventArgs e)
        {
            string temp = "";
            Result2 = list1.Distinct().ToList();
            foreach (string s in Result2)
            {
                string qn = "" + s[s.Length - 1];
                for (int i = 0; i < s.Length-1; i++ )
                {
                    temp += s[i];
                }
                    Label2.Text += "Product:" + temp + " Quantity: " + qn + "<br />";
                    finalprodname.Add(temp);
                    finalprodquan.Add(qn);
                    temp = "";
            }
            ordersubmit_Click(null, null);
        }

        protected void searchbtn_Click(object sender, EventArgs e)
        {
            SqlDataSource1.SelectCommand = "SELECT productname from product where productname LIKE '%" + searchbox.Text + "%'";
            SqlDataSource1.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["GenerateOrderGridViewConnectionString"].ConnectionString;
            GridView1.DataSource = SqlDataSource1;
            GridView1.DataBind();
        }


        

       
    }
}