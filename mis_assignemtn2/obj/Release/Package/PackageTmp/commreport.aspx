﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="commreport.aspx.cs" Inherits="mis_assignemtn2.commreport" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
		<title>Commision Report</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
	</head>
<body>
     <form id="form1" runat="server">
		<!-- Banner -->
			<section id="banner">
				<h2><strong>Dada Bhai Noorejee Electronics </strong></h2>
				<p><asp:Label ID="usrname_lbl" runat="server" Text=""></asp:Label></p>
                <ul class="actions">
					<li><asp:Button ID="logout_btn" runat="server" Text="Home"   class="button special big"  OnClick="logout_btn_Click" /></li>
				</ul>
			</section>
            

		<!-- One -->
         
			<section id="one" class="wrapper special">
                <asp:Panel runat="server" ID="pnlContents">
				<div class="inner">
					<header class="major">
						<h2>Employee Commision report</h2>
					</header>
					<div class="box">
                        <asp:Table ID="Table1" runat="server">
                            <asp:TableHeaderRow>
                                <asp:TableHeaderCell><center>Order ID</center></asp:TableHeaderCell>
                                <asp:TableHeaderCell><center>Order Date</center></asp:TableHeaderCell>
                                <asp:TableHeaderCell><center>Order Commision</center></asp:TableHeaderCell>
                            </asp:TableHeaderRow>
                        </asp:Table>
                        <asp:GridView ID="GridView1" runat="server" DataSourceID="SqlDataSource1" AutoGenerateColumns="False" ShowHeader="false">
                            <Columns>
                                <asp:BoundField DataField="orderid" HeaderText="orderid" SortExpression="orderid"></asp:BoundField>
                                <asp:BoundField DataField="OrderDate" HeaderText="OrderDate" SortExpression="OrderDate"></asp:BoundField>
                                <asp:BoundField DataField="Commision" HeaderText="Commision" SortExpression="Commision"></asp:BoundField>
                            </Columns>
                        </asp:GridView>
                        <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString='<%$ ConnectionStrings:GenerateOrderGridViewConnectionString %>' SelectCommand="SELECT        [order].orderid, [order].OrderDate, sales.Commision
FROM            ([order] INNER JOIN
                         sales ON [order].orderid = sales.OrderID)
WHERE        (sales.EmpID = (select empid from cuser) )"></asp:SqlDataSource>
                    </div>
                     </asp:Panel>
                <asp:Button ID="printreport" runat="server" Text="Print Report" OnClientClick = "return PrintPanel();" CssClass="button special big" />
			</section>
            


		<!-- Two -->
			

		<!-- Footer -->
			<footer id="footer" >
				<div class="copyright">
					&copy; Dada Bhai Noorejee Electronics.
				</div>
			</footer>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>
          <script type = "text/javascript">
              function PrintPanel() {
                  var panel = document.getElementById("<%=pnlContents.ClientID %>");
                  var printWindow = window.open('', '', 'height=400,width=800');
                  printWindow.document.write('<html><head><title>Dada Bhai Noorjee Electronics</title>');
                  printWindow.document.write('</head><body ><center>');
                  printWindow.document.write(panel.innerHTML);
                  printWindow.document.write('</center></body></html>');
                  printWindow.document.close();
                  setTimeout(function () {
                      printWindow.print();
                  }, 500);
                  return false;
              }
    </script>
        </form>
	</body>
</body>
</html>
