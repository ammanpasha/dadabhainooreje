﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="test.aspx.cs" Inherits="mis_assignemtn2.test"  MaintainScrollPositionOnPostBack="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
		<title>Typify by TEMPLATED</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
	</head>
<body>
        <form id="form1" runat="server">
		<!-- Banner -->
			<section id="banner">
				<h2><strong>Dada Bhai Noorejee Electronics </strong></h2>
				<p><asp:Label ID="usrname_lbl" runat="server" Text=""></asp:Label></p>
                <ul class="actions">
					<li><asp:Button ID="logout_btn" runat="server" Text="Logout"   class="button special big"  /></li>
				</ul>
			</section>
            

		<!-- One -->
			<section id="one" class="wrapper special">
				<div class="inner">
					<header class="major">
						<h2>Order Creation</h2>
					</header>
					<div class="box">
                         <label><b>Select Customer</b></label>
                         <asp:DropDownList ID="cust_list" runat="server" ></asp:DropDownList>
                         <label><b><br />Select Products<br /></b></label>
                        <asp:TextBox ID="searchbox" runat="server" placeholder="Search Products"></asp:TextBox>
                        <asp:Button ID="searchbtn" runat="server" Text="Button" class="button special" OnClick="searchbtn_Click" />
                     <div  class="box">
                        
           <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" PageSize="5"  OnRowDataBound="GridView1_RowDataBound" style="margin-top: 15px" Width="99%" OnRowCommand="GridView1_RowCommand" AllowPaging="True" OnSelectedIndexChanged="GridView1_SelectedIndexChanged1" ShowHeader="false" OnPageIndexChanging="GridView1_PageIndexChanging">
            <Columns> 
                
                <asp:BoundField DataField="ProductName" HeaderText="ProductName" SortExpression="ProductName" >
                 <HeaderStyle HorizontalAlign="Right" BackColor="#00CDCF" />
                 </asp:BoundField>
                <asp:TemplateField>
                     <HeaderTemplate>
                         <center>Select Quantity</center>
                     </HeaderTemplate>
                     <ItemTemplate>
                          <asp:DropDownList ID="DropDownList1" runat="server">  
                        </asp:DropDownList>
                    </ItemTemplate> 
                     <HeaderStyle BackColor="#00CDCF" HorizontalAlign="Center" />
                </asp:TemplateField>
                 <asp:TemplateField ShowHeader="False">
                     <HeaderTemplate>
                     </HeaderTemplate>
                     <ItemTemplate>
                         <asp:Button ID="AddButton" runat="server" 
                         CommandName="AddToCart" 
                         CommandArgument="<%# ((GridViewRow) Container).RowIndex %>"
                         Text="Add to Cart"
                          class="button special" />
                        <asp:CheckBox ID="chkRow" runat="server" class="button special big" Visible="false"/>
                        
                    </ItemTemplate> 
                     <HeaderStyle  />
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
                         
        <br />
        
     
                    <p>
                        &nbsp;</p>
        <p>
            <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="press kro d0st" />
        </p>
        <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label>
                         <asp:Button ID="testbtn" runat="server" Text="Button"  OnClick="testbtn_Click" />
                         <asp:Label ID="Label3" runat="server" Text="Label"></asp:Label>
                         </div>
			</section>


		<!-- Two -->
			

		<!-- Footer -->
			<footer id="footer" >
				<div class="copyright">
					&copy; Dada Bhai Noorejee Electronics.
				</div>
			</footer>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>
        </form>
	</body>
</html>
