﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="adminlogin.aspx.cs" Inherits="mis_assignemtn2.adminlogin" enableEventValidation="false" %>

<!DOCTYPE html>

<!DOCTYPE HTML>
<!--
	Typify by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
	<head>
		<title>Admin Login</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
	</head>
	<body>
        <form id="form1" runat="server">
		<!-- Banner -->
			<section id="banner">
				<h2><strong>Dada Bhai Noorejee Electronics </strong></h2>
				<p>Admin Login Page</p>
			<asp:Label ID="Label1" runat="server" Text=""></asp:Label>
			</section>


			

		<!-- Two -->
			<section id="two" class="wrapper style2 special">
				<div class="inner narrow">
					<header>
						<h2>Login</h2>
					</header>
					<form class="grid-form" method="post" action="#">
						<div class="form-control narrow">
							<label for="name">Username</label>
                            <asp:TextBox ID="usrname_txtbox" runat="server"></asp:TextBox>
						</div>
						<div class="form-control narrow">
							<label for="email">Password</label>
							<asp:TextBox ID="pwd_txtbox" runat="server" TextMode="Password"></asp:TextBox>
						</div>
                        <br />
						<ul class="actions">
                            <li><asp:Button ID="submit" runat="server" OnClick="submit_Click" Text="Login" /></li>
						</ul>
					</form>
				</div>
			</section>
 
		<!-- Footer -->
			<footer id="footer">
				<div class="copyright">
					&copy; Dada Bhai Noorejee Electronics.
				</div>
			</footer>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>
        </form>
	</body>
</html>
