﻿<%@ Page Language="C#" AutoEventWireup="true" enableEventValidation="false" CodeBehind="addcust.aspx.cs" Inherits="mis_assignemtn2.addcust" %>

<!DOCTYPE html>
<html>

	<head>
		<title>Add a New Customer</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
	</head>
	<body>
        <form id="form1" runat="server">
		<!-- Banner -->
			<section id="banner">
				<h2><strong>Dada Bhai Noorejee Electronics </strong></h2>
				<p><asp:Label ID="usrname_lbl" runat="server" Text="" Font-Bold="true"></asp:Label></p>
                <ul class="actions">
					<li><asp:Button ID="logout_btn" runat="server" Text="Logout"  OnClick="logout_btn_Click" class="button special big" /></li>
				</ul>
			</section>
            

		<!-- One -->
             

		<!-- Two -->
			

            	<section id="two" class="wrapper style2 special">
				<div class="inner narrow">
                    <div class="inner">
					<header class="major">
						<h2>Add a New Customer</h2>
					</header> 
                    </div>
					<form class="grid-form" method="post" action="#">
						<div class="form-control narrow">
							<label for="name">Customer Name</label>
                            <asp:TextBox ID="cname_txtbox" runat="server"></asp:TextBox>
						</div>
                        <br />
                        <div class="form-control narrow">
							<label for="name">Address</label>
                            <asp:TextBox ID="adr_txtbox" runat="server"></asp:TextBox>
						</div>
                        <br />
                        <div class="form-control narrow">
							<label for="name">Phone Number</label>
                            <asp:TextBox ID="ph_txtbox" runat="server"></asp:TextBox>
						</div>
                        <br />
						<ul class="actions">
                            <li><asp:Button ID="submit" runat="server"  OnClick="submit_Click" Text="Submit" /></li>
						</ul>
					</form>
				</div>
			</section>


		<!-- Footer -->
			<footer id="footer" >
				<div class="copyright">
					&copy; Dada Bhai Noorejee Electronics.
				</div>
			</footer>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>
        </form>
	</body>
</html>

