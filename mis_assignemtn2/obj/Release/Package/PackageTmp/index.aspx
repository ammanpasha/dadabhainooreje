﻿    <%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" enableEventValidation="false" Inherits="mis_assignemtn2.WebForm1" %>

<!DOCTYPE html>

<!DOCTYPE HTML>
<!--
	Typify by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
	<head>
		<title>Dada Bhai Noorejee Electronics</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
	</head>
	<body>
        <form id="form1" runat="server">
		<!-- Banner -->
			<section id="banner">
				<h2><strong>Dada Bhai Noorejee Electronics </strong></h2>
				
			</section>
            

		<!-- One -->
			<section id="one" class="wrapper special">
				<div class="inner">
                    <header class="major">
						<h2>Welcome!</h2>
					</header>
                    <ul class="actions">
					<li><asp:Button ID="Button1" runat="server" Text="Admin Login" class="button special" OnClick="Button1_Click"/></li>
                    <li><asp:Button ID="Button2" runat="server" Text="Employee Login" class="button special" OnClick="emplogin_Click"  /></li>
                    <li><asp:Button ID="Button3" runat="server" Text="Employee Signup" class="button special" OnClick="Button3_Click"/></li>
				</ul>
                    </div>
                </section>

		<!-- Two -->
			

		<!-- Footer -->
			<footer id="footer" >
				<div class="copyright">
					&copy; Dada Bhai Noorejee Electronics.
				</div>
			</footer>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>
        </form>
	</body>
</html>
