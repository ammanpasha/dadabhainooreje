﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="prodrep.aspx.cs" Inherits="mis_assignemtn2.prodrep" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
		<title>Product Sales Report</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css"  media="screen"/>
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
	</head>
<body>
     <form id="form1" runat="server">
		<!-- Banner -->
			<section id="banner">
				<h2><strong>Dada Bhai Noorejee Electronics </strong></h2>
				<p><asp:Label ID="usrname_lbl" runat="server" Text=""></asp:Label></p>
                <ul class="actions">
					<li><asp:Button ID="logout_btn" runat="server" Text="Home"   class="button special big" OnClick="logout_btn_Click"  /></li>
				</ul>
			</section>
            

		<!-- One -->
			<section id="one" class="wrapper special">
				<div class="inner">
					<header class="major">
						<h2>product Sales report</h2>
					</header>
                    <ul class="actions">
					<li><label>Select Product: </label></li>
                        <li> <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="SqlDataSource2" DataTextField="ProductID" DataValueField="ProductID" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged1" AutoPostBack="true"></asp:DropDownList><asp:SqlDataSource runat="server" ID="SqlDataSource2" ConnectionString='<%$ ConnectionStrings:GenerateOrderGridViewConnectionString %>' SelectCommand="SELECT [ProductID] FROM [product]"></asp:SqlDataSource>
                        </li>
				</ul>
                    <asp:Panel runat="server" ID="pnlContents">
                    <div class="box">
                        <asp:GridView ID="GridView1" runat="server" ShowHeader="true" AutoGenerateColumns="False" DataSourceID="SqlDataSource1">
                            <Columns>
                                <asp:BoundField DataField="ProductID" HeaderText="ProductID" SortExpression="ProductID"></asp:BoundField>
                                <asp:BoundField DataField="Product Name" HeaderText="Product Name" ReadOnly="True" SortExpression="Product Name" />
                                <asp:BoundField DataField="Quantity Sold" HeaderText="Quantity Sold" ReadOnly="True" SortExpression="Quantity Sold"></asp:BoundField>
                                <asp:BoundField DataField="Total Sales Revenue" HeaderText="Total Sales Revenue" ReadOnly="True" SortExpression="Total Sales Revenue" />
                            </Columns>
                        </asp:GridView>
                        <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString='<%$ ConnectionStrings:GenerateOrderGridViewConnectionString %>' SelectCommand="SELECT        product.ProductID,  
(select product.productname from product where productid = @) as [Product Name],
SUM(orderproduct.Quantity) AS [Quantity Sold],
(select SUM(orderproduct.Quantity)*product.productprice from product where productid = @) as [Total Sales Revenue]
FROM            (([order] INNER JOIN
                         orderproduct ON [order].orderid = orderproduct.OrderID) INNER JOIN
                         product ON orderproduct.ProductID = product.ProductID)
WHERE product.productid = @
GROUP BY product.productid">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="DropDownList1" PropertyName="SelectedValue" DefaultValue="1"></asp:ControlParameter>


                            </SelectParameters>
                        </asp:SqlDataSource>
                    </div>
                        </asp:Panel>
                    <asp:Button ID="printreport" runat="server" Text="Print Report" OnClientClick = "return PrintPanel();" CssClass="button special big" />
			</section>


		<!-- Two -->
			

		<!-- Footer -->
			<footer id="footer" >
				<div class="copyright">
					&copy; Dada Bhai Noorejee Electronics.
				</div>
			</footer>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>
          <script type = "text/javascript">
              function PrintPanel() {
                  var panel = document.getElementById("<%=pnlContents.ClientID %>");
                  var printWindow = window.open('', '', 'height=400,width=800');
                     printWindow.document.write('<html><head><title>Dada Bhai Noorjee Electronics</title>');
                     printWindow.document.write('</head><body ><center>');
                     printWindow.document.write(panel.innerHTML);
                     printWindow.document.write('</center></body></html>');
                     printWindow.document.close();
                     setTimeout(function () {
                         printWindow.print();
                     }, 500);
                     return false;
                 }
    </script>
        </form>
	</body>
</body>
</html>
