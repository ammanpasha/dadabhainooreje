﻿<%@ Page Language="C#" AutoEventWireup="true" enableEventValidation="false"  CodeBehind="gorder.aspx.cs" Inherits="mis_assignemtn2.gorder" MaintainScrollPositionOnPostBack="true" %>

<!DOCTYPE HTML>
 
<html>
	<head>
		<title>Generate Order</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
	</head>
	<body>
        <form id="form1" runat="server">
		<!-- Banner -->
			<section id="banner">
				<h2><strong>Dada Bhai Noorejee Electronics </strong></h2>
				<p><asp:Label ID="usrname_lbl" runat="server" Text=""></asp:Label></p>
                <ul class="actions">
					<li><asp:Button ID="logout_btn" runat="server" Text="Logout"   class="button special big" OnClick="logout_btn_Click" /></li>
				</ul>
			</section>
            

		<!-- One -->
			<section id="one" class="wrapper special">
				<div class="inner">
					<header class="major">
						<h2>Order Creation</h2>
					</header>
					<div class="box">
                         <label><b>Select Customer</b></label>
                         <asp:DropDownList ID="cust_list" runat="server" ></asp:DropDownList>
                         <label><b><br />Select Products<br /></b></label>
                        <asp:Panel ID="Panel1" runat="server" DefaultButton="searchbtn">
                            <ul class="actions">
                            <li><asp:TextBox ID="searchbox" runat="server" placeholder="Search Products"></asp:TextBox></li>
                            <li><asp:Button ID="searchbtn" runat="server" Text="Search" class="button special"  OnClick="searchbtn_Click"/></li>
                        </ul>
                        </asp:Panel>
                        
                     <div  class="box">
                        
           <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" PageSize="5"  OnRowDataBound="GridView1_RowDataBound" style="margin-top: 15px" Width="99%" OnRowCommand="GridView1_RowCommand" AllowPaging="True" ShowHeader="false" OnPageIndexChanging="GridView1_PageIndexChanging">
            <Columns> 
                
                <asp:BoundField DataField="ProductName" HeaderText="ProductName" SortExpression="ProductName" >
                 <HeaderStyle HorizontalAlign="Right" BackColor="#00CDCF" />
                 </asp:BoundField>
                <asp:TemplateField>
                     <HeaderTemplate>
                         <center>Select Quantity</center>
                     </HeaderTemplate>
                     <ItemTemplate>
                          <asp:DropDownList ID="DropDownList1" runat="server">  
                        </asp:DropDownList>
                    </ItemTemplate> 
                     <HeaderStyle BackColor="#00CDCF" HorizontalAlign="Center" />
                </asp:TemplateField>
                 <asp:TemplateField ShowHeader="False">
                     <HeaderTemplate>
                     </HeaderTemplate>
                     <ItemTemplate>
                         <asp:Button ID="AddButton" runat="server" 
                         CommandName="AddToCart" 
                         CommandArgument="<%# ((GridViewRow) Container).RowIndex %>"
                         Text="Add to Cart"
                          class="button special" />
                        <asp:CheckBox ID="chkRow" runat="server" class="button special big" Visible="false"/>
                        
                    </ItemTemplate> 
                     <HeaderStyle  />
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
                    </div>
                        <asp:Button ID="ordersubmit" runat="server" Text="Submit Order" class="button special" visible="false" OnClick="ordersubmit_Click" />
                        <asp:Label ID="shit" runat="server" Text="Label" Visible="false"></asp:Label>
                         <br />
                         <br />
                         <asp:Button ID="cartbtn" runat="server" Text="Submit Order" OnClick="cartbtn_Click" class="button special" />
                         <br />
                         <asp:Label ID="Label1" runat="server" visible="false" Text="In Your Cart:" ></asp:Label>
                         <br />
                         <asp:Label ID="Label2" runat="server" visible="false" Text="Label"  ></asp:Label>
                         </div>
			</section>


		<!-- Two -->
			

		<!-- Footer -->
			<footer id="footer" >
				<div class="copyright">
					&copy; Dada Bhai Noorejee Electronics.
				</div>
			</footer>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>
        </form>
	</body>
</html>
