﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="eportal.aspx.cs" Inherits="mis_assignemtn2.eportal" enableEventValidation="false" %>

<!DOCTYPE HTML>
<!--
	Typify by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
	<head>
		<title>Employee Portal</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
	</head>
	<body>
        <form id="form1" runat="server">
		<!-- Banner -->
			<section  id="banner">
				<h2><strong>Dada Bhai Noorejee Electronics </strong></h2>
				<p><asp:Label ID="usrname_lbl" runat="server" Text=""></asp:Label></p>
                <ul class="actions">
					<li><asp:Button ID="logout_btn" runat="server" Text="Logout"  OnClick="logout_btn_Click" class="button special big" /></li>
				</ul>
			</section>
            

		<!-- One -->
			<section id="one" class="wrapper special">
				<div class="inner">
					<header class="major">
						<h2>Employee Panel</h2>
					</header>
					<div class="features">
                <table>
                    <tr>
                        <td><asp:Button ID="generateOrder" runat="server" Text="Generate New Order"  OnClick="generateOrder_Click" class="button special" Width="20em"/></td>
                    </tr>
                    <tr>
                        <td>
                             <asp:Button ID="existOrder" runat="server" Text="View Existing Orders"  OnClick="existOrder_Click" class="button special " Width="20em"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="prodrep" runat="server" Text="View Product Sales Report"   class="button special" OnClick="prodrep_Click" Width="20em" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="comrep" runat="server" Text="View Commision Report"  class="button special" OnClick="comrep_Click" Width="20em" /> 
                        </td>
                    </tr>
                </table>
            </div>
                    </div>
			</section>


		<!-- Two -->
			

		<!-- Footer -->
			<footer id="footer" >
				<div class="copyright">
					&copy; Dada Bhai Noorejee Electronics.
				</div>
			</footer>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>
        </form>
	</body>
</html>

