﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace mis_assignemtn2
{
      
    public partial class test : System.Web.UI.Page
    {
        SqlDataSource SqlDataSource1 = new SqlDataSource();
        public static List<string> list1 = new List<string>();
        String[] str = new String[100];
        string str1;
        protected void Page_Load(object sender, EventArgs e)
        {
           if(!IsPostBack)
           {

               SqlDataSource1.ID = "SqlDataSource1";
               this.Page.Controls.Add(SqlDataSource1);
               SqlDataSource1.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["GenerateOrderGridViewConnectionString"].ConnectionString;
               SqlDataSource1.SelectCommand = "SELECT productname from product";
               GridView1.DataSource = SqlDataSource1;
               GridView1.DataBind();
           }
       
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            GridView1.DataBind(); //fazool
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DropDownList DropDownList1 = (e.Row.FindControl("DropDownList1") as DropDownList);
                DropDownList1.Items.Add("Select Quantity");
                DropDownList1.Items.Add("1");
                DropDownList1.Items.Add("2");
                DropDownList1.Items.Add("3");
                DropDownList1.Items.Add("4");
                DropDownList1.Items.Add("5");
                DropDownList1.Items.Add("6");
                DropDownList1.Items.Add("7");
                DropDownList1.Items.Add("8");
                DropDownList1.Items.Add("9");
                DropDownList1.Items.Add("10");
            }
            
        }

        
        protected void Button2_Click(object sender, EventArgs e)
        {
            //Label2.Text += "<br />===========================================================================================<br />";
            foreach (GridViewRow row in GridView1.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox chkRow = (row.Cells[0].FindControl("chkRow") as CheckBox);
                    if (chkRow.Checked)
                    {
                        string name = (row.Cells[0].FindControl("DropDownList1") as DropDownList).Text;
                        int ind = GetColumnIndexByName(GridView1, "ProductName");

                        string country = row.Cells[ind].Text;
                        //Label2.Text += "<br />-----------------<br />You've selected frm dropdown: " + name + "<br />ProductName:  " + country + "<br />-----------------<br />";
                        //Label2.Text = "";

                           list1.Add(country + name);
                            str1 += country + name +",";
                        
                       foreach(string s in list1)
                       {
                           //Label2.Text += s + "<br />";
                       }
                       Label2.Text = str1;

                    }
                }
            }
        }

        private int GetColumnIndexByName(GridView grid, string name)
        {
            foreach (DataControlField col in grid.Columns)
            {
                if (col.HeaderText.ToLower().Trim() == name.ToLower().Trim())
                {
                    return grid.Columns.IndexOf(col);
                }
            }

            return -1;
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            
            //GridView1.Rows
           // Button btn3 = rw.Cells[0].FindControl("Button3") as Button;
          //  btn3.Text = "Added";
           

        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "AddToCart")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                GridViewRow row = GridView1.Rows[index];
                Button btn = row.Cells[0].FindControl("AddButton") as Button;
               if(btn.Text != "Added!")
               {
                   btn.Text = "Added!";
                   CheckBox chkRow = (row.Cells[0].FindControl("chkRow") as CheckBox);
                   chkRow.Checked = true;
                   Button2_Click(null, null);
               }
               else
               {
                   btn.Text = "Add to Cart";
                   CheckBox chkRow = (row.Cells[0].FindControl("chkRow") as CheckBox);
                   chkRow.Checked = false;
               }
                
            }
        }

        protected void GridView1_SelectedIndexChanged1(object sender, EventArgs e)
        {

        }

        protected void searchbtn_Click(object sender, EventArgs e)
        {
            SqlDataSource1.SelectCommand = "SELECT productname from product where productname LIKE '%" + searchbox.Text + "%'";
            SqlDataSource1.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["GenerateOrderGridViewConnectionString"].ConnectionString;
            GridView1.DataSource = SqlDataSource1;
            GridView1.DataBind();

            Label2.Text = "SELECT productname from product where productname LIKE '%" + searchbox.Text + "%'";
        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            SqlDataSource1.SelectCommand = "SELECT productname from product";
            SqlDataSource1.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["GenerateOrderGridViewConnectionString"].ConnectionString;
            GridView1.DataSource = SqlDataSource1;
            GridView1.DataBind();
            //GridView1.DataBind();
        }

        protected void showitem(object sender, GridViewPageEventArgs e)
        {
            foreach (string s in list1)
            {
                Label3.Text += s + "<br />";
            }
        }

        protected void testbtn_Click(object sender, EventArgs e)
        {
            Label3.Text = "In Your Cart: <br />";
            List<string> Result2 = list1.Distinct().ToList();
            foreach (string s in Result2)
            {
                string qn = ""+s[s.Length-1];
                Label3.Text += "Product:" + s + " Quantity: " +  qn + "<br />";
            }
        }
    }
}