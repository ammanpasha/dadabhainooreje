﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Text;

namespace mis_assignemtn2
{
    public partial class ordergenok : System.Web.UI.Page
    {
        SqlConnection conn = new SqlConnection();
        string constr = @"Server=tcp:dbnje.database.windows.net,1433;Initial Catalog=mydb;Persist Security Info=False;User ID=amman;Password=Pasha123_;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
        SqlCommand cmd = new SqlCommand();
        string ufname = "";
        string ulname = "";
        int id; //empid
        int cid; //customer id
        int pid; //product id
        int oid; //order id
        string query;
        protected void Page_Load(object sender, EventArgs e)
        {
            //getting orderid from querystring
            oid = Convert.ToInt32(Request.QueryString["oid"]);
            conn.ConnectionString = constr;
            conn.Open();
            cmd.Connection = conn;
            //getting empid 
            cmd.CommandText = "select empid from cuser";
            cmd.Connection = conn;
            id = (int)cmd.ExecuteScalar(); 
            cmd.CommandText = "select [fname] from [emp] where [empid] = " + id + "";
            ufname = (string)cmd.ExecuteScalar(); 
            cmd.CommandText = "select [lname] from [emp] where [empid] = " + id + "";
            ulname += (string)cmd.ExecuteScalar();
            usrname_lbl.Text = "Logged-in as: " + ufname + " " + ulname + "<br />Employee Code: E00" + id;
            usrname_lbl.Font.Bold = true;
           // Label1.Text = oid + " <-- passed from qstring";
            oid_lbl.Text = "Order ID: O-00" + oid;
            cmd.CommandText = "SELECT OrderDate FROM [order] WHERE (orderid = " + oid +")";
            odate_lbl.Text = "Order Date: " + Convert.ToString(cmd.ExecuteScalar());
            cmd.CommandText = "SELECT Commision FROM sales WHERE (OrderID = " + oid +") AND (EmpID = "+ id +")";
            comm_lbl.Text = "Employee Commision: " + Convert.ToString(cmd.ExecuteScalar());
            cmd.CommandText = "SELECT OrderTotal FROM [order] WHERE (orderid = " + oid + ")";
            oprice_lbl.Text = "Order Total: " + Convert.ToString(cmd.ExecuteScalar());
            cmd.CommandText = "select custid from [order] where orderid = " + oid + "";
            cnum_lbl.Text = "Customer ID: C-00" + Convert.ToString(cmd.ExecuteScalar());
            cmd.CommandText = "SELECT customer.CustName FROM (customer INNER JOIN [order] ON customer.CustID = [order].custid) WHERE [order].orderid = " + oid;
            cname_lbl.Text = "Customer Name: " + Convert.ToString(cmd.ExecuteScalar());
            ename_lbl.Text = "Order Collected By: " + ufname + " " + ulname;
            enum_lbl.Text = "Employee Number: E00" + id;
            conn.Close();

        }

        protected void logout_btn_Click(object sender, EventArgs e)
        {
            conn.ConnectionString = constr;
            conn.Open();
            string query = "delete from cuser";
            cmd.Connection = conn;
            cmd.CommandText = query;
            cmd.ExecuteNonQuery();
            conn.Close();
            Response.Redirect("index.aspx");
        }

        protected void home_btn_Click(object sender, EventArgs e)
        {
            Response.Redirect("eportal.aspx");
        }
    }
}