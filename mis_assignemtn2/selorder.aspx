﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="selorder.aspx.cs" Inherits="mis_assignemtn2.selorder" %>

<!DOCTYPE html>

<!DOCTYPE HTML>
 
<html>
	<head>
		<title>Select Order</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
	</head>
	<body>
        <form id="form1" runat="server">
		<!-- Banner -->
			<section id="banner">
				<h2><strong>Dada Bhai Noorejee Electronics </strong></h2>
				<p><asp:Label ID="usrname_lbl" runat="server" Text=""></asp:Label></p>
                <ul class="actions">
					<li><asp:Button ID="logout_btn" runat="server" Text="Logout"   class="button special big"   OnClick="logout_btn_Click"/></li>
                    <li><asp:Button ID="home_btn" runat="server" Text="Home"   class="button special big"    OnClick="home_btn_Click"/></li>
				</ul>
			</section>
            

		<!-- One -->
			<section id="one" class="wrapper special">
				<div class="inner">
					<header class="major">
						<h2>Select ORder</h2>
					</header>
                    <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="SqlDataSource2" DataTextField="OrderID" DataValueField="OrderID"></asp:DropDownList>
                    <asp:SqlDataSource runat="server" ID="SqlDataSource2" ConnectionString='<%$ ConnectionStrings:GenerateOrderGridViewConnectionString %>' ProviderName='<%$ ConnectionStrings:GenerateOrderGridViewConnectionString.ProviderName %>' SelectCommand="SELECT OrderID FROM sales WHERE (EmpID = (SELECT empid FROM cuser))"></asp:SqlDataSource>
                    <br /><asp:Button ID="report_btn" runat="server" Text="View Report"   class="button special big"  OnClick="report_btn_Click"  />
                         </div>
			</section>


		<!-- Two -->
			

		<!-- Footer -->
			<footer id="footer" >
				<div class="copyright">
					&copy; Dada Bhai Noorejee Electronics.
				</div>
			</footer>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>
        </form>
	</body>
</html>
