﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ordergenok.aspx.cs" Inherits="mis_assignemtn2.ordergenok" %>

<!DOCTYPE html>

<!DOCTYPE HTML>
 
<html>
	<head>
		<title>Order Report</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css"  media="screen"/>
        <link rel="stylesheet" href="assets/css/main.css"   media="print"/>
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
	</head>
	<body>
        <form id="form1" runat="server">
		<!-- Banner -->
			<section id="banner">
				<h2><strong>Dada Bhai Noorejee Electronics </strong></h2>
				<p><asp:Label ID="usrname_lbl" runat="server" Text=""></asp:Label></p>
                <ul class="actions">
					<li><asp:Button ID="logout_btn" runat="server" Text="Logout"   class="button special big"  OnClick="logout_btn_Click"/></li>
                    <li><asp:Button ID="home_btn" runat="server" Text="Home"   class="button special big"   OnClick="home_btn_Click"/></li>
				</ul>
			</section>
            

		<!-- One -->
			<section id="one" class="wrapper special">
                
				<div class="box inner" id="#rep">
					<header class="major">
						<h2>Order Report</h2>
					</header>
                    <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                    <asp:Panel ID="pnlContents" runat="server" CssClass="box">
                    <div class="major">
                         <h3><asp:Label ID="oid_lbl" runat="server" Text="Label" Font-Size="XX-Large" Font-Bold="True"></asp:Label></h3><br />
                         <h3><asp:Label ID="odate_lbl" runat="server" Text="Label" Font-Size="XX-Large" Font-Bold="True"></asp:Label></h3><br />
                         <h3> <asp:Label ID="oprice_lbl" runat="server" Text="Label" Font-Size="XX-Large" Font-Bold="True"></asp:Label></h3><hr />
                         <h3> <asp:Label ID="cname_lbl" runat="server" Text="Label" Font-Size="XX-Large" Font-Bold="True"></asp:Label></h3><br />
                         <h3> <asp:Label ID="cnum_lbl" runat="server" Text="Label" Font-Size="XX-Large" Font-Bold="True"></asp:Label></h3><hr />
                        <h3>Products in Order: </h3>
                       <div class="gridamman"> 
                           <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" ShowHeader="true" DataSourceID="SqlDataSource1">
                            <Columns>
                                <asp:BoundField DataField="ProductID" HeaderText="Product ID" SortExpression="ProductID"></asp:BoundField>
                                    <asp:BoundField DataField="ProductName" HeaderText="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Product Name" SortExpression="ProductName" HeaderStyle-HorizontalAlign="Left"></asp:BoundField>
                                <asp:BoundField DataField="Unit Price" HeaderText="Unit Price" SortExpression="Unit Price"></asp:BoundField>
                                <asp:BoundField DataField="Quantity" HeaderText="Quantity" SortExpression="Quantity"></asp:BoundField>
                                <asp:BoundField DataField="Extended Price" HeaderText="Extended Price" ReadOnly="True" SortExpression="Extended Price"></asp:BoundField>
                            </Columns>
                        </asp:GridView>
                           </div>
                        <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString='<%$ ConnectionStrings:GenerateOrderGridViewConnectionString %>' SelectCommand="SELECT        product.ProductID, product.ProductName, product.ProductPrice as [Unit Price], orderproduct.Quantity, product.ProductPrice*orderproduct.Quantity as [Extended Price]
FROM            (orderproduct INNER JOIN
                         product ON orderproduct.ProductID = product.ProductID)
WHERE        (orderproduct.OrderID = @)">
                            <SelectParameters>
                                <asp:QueryStringParameter QueryStringField="oid"></asp:QueryStringParameter>
                            </SelectParameters>
                        </asp:SqlDataSource>
                        <hr />
                        <h3> <asp:Label ID="ename_lbl" runat="server" Text="Label" Font-Size="XX-Large" Font-Bold="True"></asp:Label></h3><br />
                         <h3> <asp:Label ID="enum_lbl" runat="server" Text="Label" Font-Size="XX-Large" Font-Bold="True"></asp:Label></h3><br />
                         <h3> <asp:Label ID="comm_lbl" runat="server" Text="Label" Font-Size="XX-Large" Font-Bold="True"></asp:Label></h3><br /><hr />
                      </div>
                        </asp:Panel>
                  </div>
                    
                <asp:Button ID="printreport" runat="server" Text="Print Report" OnClientClick = "return PrintPanel();" CssClass="button special big" />
			</section>


		<!-- Two -->
			

		<!-- Footer -->
			<footer id="footer" >
				<div class="copyright">
					&copy; Dada Bhai Noorejee Electronics.
				</div>
			</footer>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>
             <script type = "text/javascript">
                 function PrintPanel() {
                     var panel = document.getElementById("<%=pnlContents.ClientID %>");
                     var printWindow = window.open('', '', 'height=400,width=800');
                        printWindow.document.write('<html><head><title>Dada Bhai Noorjee Electronics</title>');
                        printWindow.document.write('</head><body ><center><h1>Order Report</h1><hr />');
                        printWindow.document.write(panel.innerHTML);
                        printWindow.document.write('</center></body></html>');
                        printWindow.document.close();
                        setTimeout(function () {
                            printWindow.print();
                        }, 500);
            return false;
        }
    </script>
        </form>
	</body>
</html>
