﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="adminportal.aspx.cs" Inherits="mis_assignemtn2.adminportal" %>

<!DOCTYPE html>

<html>
	<head>
		<title>Admin Portal</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
	</head>
	<body>
        <form id="form1" runat="server">
		<!-- Banner -->
			<section id="banner">
				<h2><strong>Dada Bhai Noorejee Electronics </strong></h2>
				<p><asp:Label ID="usrname_lbl" runat="server" Text=""></asp:Label></p>
                <ul class="actions">
					<li><asp:Button ID="logout_btn" runat="server" Text="Logout"   OnClick="logout_btn_Click" class="button special big" /></li>
				</ul>
			</section>
            

		<!-- One -->
			<section id="one" class="wrapper special">
				<div class="inner">
					<header class="major">
						<h2>Admin Panel</h2>
					</header>
					<div class="features">
                         
					       
                         
					        
                            
                <table>
                    <tr>
                        <td><asp:Button ID="addProd" runat="server" Text="Enter New Product" OnClick="addProd_Click" class="button special" /></td>
                    </tr>
                    <tr>
                        <td>
                             <asp:Button ID="addCust" runat="server" Text="Register New Customer"  OnClick="addCust_Click" class="button special " />
                        </td>
                    </tr>
                </table>
            </div>
			</section>


		<!-- Two -->
			

		<!-- Footer -->
			<footer id="footer" >
				<div class="copyright">
					&copy; Dada Bhai Noorejee Electronics.
				</div>
			</footer>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>
        </form>
	</body>
</html>
