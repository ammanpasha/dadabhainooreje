﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace mis_assignemtn2
{
    public partial class adminportal : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void logout_btn_Click(object sender, EventArgs e)
        {
            Response.Redirect("default.aspx");
        }

        protected void generateOrder_Click(object sender, EventArgs e)
        {

        }

        protected void addProd_Click(object sender, EventArgs e)
        {
            Response.Redirect("addprod.aspx");
        }

        protected void addCust_Click(object sender, EventArgs e)
        {
            Response.Redirect("addcust.aspx");
        }
    }
}